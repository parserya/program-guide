import React from 'react';

import {Switch, Route, Redirect} from 'react-router-dom';

import {ProgramPage} from './Program';
import {ChannelPage} from './Channel';
import {ChannelsListPage} from './ChannelsList';

export const useRoutes = () => (
    <Switch>
        <Route path="/:city/:thid/:chid/info">
            <ChannelPage />
        </Route>
        <Route path="/:city/:thid/:chid">
            <ProgramPage />
        </Route>
        <Route path="/:city/:thid">
            <ChannelsListPage />
        </Route>
        <Redirect to="/ekat/1" />
    </Switch>
)