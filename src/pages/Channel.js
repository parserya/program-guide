import React, {lazy, Suspense, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';

import Loading from '../components/Loading';
import {fetchChannels} from '../store/actions/channels';
const ChannelBlock = lazy(() => import('../components/ChannelBlock'));

export const ChannelPage = () => {
    const chid = useParams().chid;
    const city = useParams().city;
    const thid = useParams().thid;

    const dispatch = useDispatch();
    const channels = useSelector(state => state.channels.channels);
    const badRequest = useSelector(state => state.channels.badRequest);

    useEffect(() => {
        if (!channels[city] || !channels[city][chid])
            dispatch(fetchChannels(chid, city));
    }, [dispatch, chid, city, channels]);

    if (badRequest) {
        return <h1>Страница не найдена</h1>
    }

    return (
        <Suspense fallback={<Loading />}>
            <ChannelBlock
                channels={channels}
                chid={chid}
                city={city}
                thid={thid}
                badRequest={badRequest}
            />
        </Suspense>
    )
}