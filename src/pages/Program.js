import React, {lazy, Suspense, useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';

import dayjs from 'dayjs';

import Loading from '../components/Loading';
import {fetchChannels} from '../store/actions/channels';
import {clearPrograms, fetchPrograms} from '../store/actions/programs';
import {setCurrentDate} from '../store/actions/app';

const ProgramBlock = lazy(() => import('../components/ProgramBlock'));

export const ProgramPage = () => {
    const [xvid, setXvid] = useState();

    const chid = useParams().chid;
    const city = useParams().city;
    const thid = useParams().thid;

    const dispatch = useDispatch();
    const channels = useSelector(state => state.channels.channels);
    const programs = useSelector(state => state.programs.programs);
    const negativeOffset = useSelector(state => state.app.negativeOffset);
    const collapsed = useSelector(state => state.app.collapsed);
    const currentDate = useSelector(state => state.app.currentDate);
    const badRequest = useSelector(state => state.channels.badRequest);

    useEffect(() => {
        if (!channels[city] || !channels[city][chid])
            dispatch(fetchChannels(chid, city));
    }, [dispatch, chid, city, channels]);

    useEffect(() => {
        if (channels[city] && channels[city][chid])
            setXvid(channels[city][chid]["xvid"])
    }, [chid, city, channels]);

    useEffect(() => {
        if (!currentDate) {
            dispatch(setCurrentDate(dayjs().format('YYYY-MM-DD')));
        }
    }, [dispatch, currentDate]);

    useEffect(() => {
        if (currentDate) {
            const today = currentDate;
            const tommorow = dayjs(currentDate).add(1, 'day').format('YYYY-MM-DD');
            if (xvid)
                dispatch(fetchPrograms(today, tommorow, xvid, city));
        }
    }, [dispatch, currentDate, chid, city, xvid]);

    useEffect(() => {
        return () => {
            dispatch(clearPrograms());
        }
    }, [dispatch]);

    if (badRequest) {
        return <h1>Страница не найдена</h1>
    }

    return (
        <Suspense fallback={<Loading/>}>
            <ProgramBlock
                channels={channels}
                programs={programs}
                chid={chid}
                city={city}
                negativeOffset={negativeOffset}
                collapsed={collapsed}
                thid={thid}
                currentDate={currentDate}
                badRequest={badRequest}
            />
        </Suspense>
    );
}