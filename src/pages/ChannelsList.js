import React, {lazy, Suspense, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';

import Loading from '../components/Loading';
import {fetchChannelsList, fetchChannelsThemes} from '../store/actions/channels';

const ChannelsListBlock = lazy(() => import('../components/ChannelsListBlock'));

export const ChannelsListPage = () => {
    const city = useParams().city;
    const thid = useParams().thid;

    const dispatch = useDispatch();
    const channels = useSelector(state => state.channels.channels);
    const themes = useSelector(state => state.channels.themes);

    useEffect(() => {
        dispatch(fetchChannelsList(city, thid));
    }, [dispatch, city, thid]);

    useEffect(() => {
        if (Object.values(themes).length === 0) {
            dispatch(fetchChannelsThemes());
        }
    }, [dispatch, themes, thid])

    return <Suspense fallback={<Loading />}>
        <ChannelsListBlock
            city={city}
            thid={thid}
            channels={channels}
            themes={themes}
        />
    </Suspense>
}