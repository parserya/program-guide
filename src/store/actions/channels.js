import {FETCH_CHANNELS, FETCH_THEMES, BAD_REQUEST} from './actionTypes';
import {API_URL} from '../../config';

export const fetchChannels = (chid, domain) => {
    return async dispatch => {
        try {
            const response = await fetch(`${API_URL}/channel/info?chid=${chid}&domain=${domain}`);
            const json = await response.json();

            if (!json.chid) {
                dispatch({
                   type: BAD_REQUEST
                });
            } else {
                dispatch({
                    type: FETCH_CHANNELS,
                    payload: {
                        [domain]: {
                            [chid]: json
                        }
                    }
                });
            }
        } catch(e) {
            console.log(e);
        }
    }
}

export const fetchChannelsList = (domain, thid) => {
    return async dispatch => {
        try {
            const response = await fetch(`${API_URL}/channel/list?domain=${domain}&thid=${thid}`);
            const json = await response.json();

            // Normalize recieved data
            const result = {};
            for (const row of json) {
                result[row.chid] = row;
            }

            dispatch({
                type: FETCH_CHANNELS,
                payload: {
                    [domain]: result
                }
            });

        } catch(e) {
            console.log(e);
        }
    }
}

export const fetchChannelsThemes = () => {
    return async dispatch => {
        try {
            const response = await fetch(`${API_URL}/channeltheme/list`);
            const json = await response.json();

            // Normalize recieved data
            const result = {};
            for (const row of json) {
                result[row.thid] = {
                    value: row.thid,
                    label: row.name
                };
            }

            dispatch({
                type: FETCH_THEMES,
                payload: result
            });
        } catch (e) {
            console.log(e);
        }
    }
}