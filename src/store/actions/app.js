import {ADD_NEGATIVE_OFFSET, RESET_NEGATIVE_OFFSET, TOGGLE_COLLAPSE, SET_CURRENT_DATE} from './actionTypes';

export const resetNegativeOffset = () => {
    return {
        type: RESET_NEGATIVE_OFFSET
    }
}

export const addNegativeOffset = (height) => {
    return {
        type: ADD_NEGATIVE_OFFSET,
        payload: height
    }
}

export const toggleCollapse = () => {
    return {
        type: TOGGLE_COLLAPSE
    }
}

export const setCurrentDate = (date) => {
    return {
        type: SET_CURRENT_DATE,
        payload: date
    }
}
