import {FETCH_PROGRAMS, CLEAR_PROGRAMS} from './actionTypes';
import {resetNegativeOffset} from './app';
import {API_URL} from '../../config';

export const fetchPrograms = (from, to, xvid, domain) => {
    return async dispatch => {
        try {
            const response = await fetch(`${API_URL}/program/list?date_from=${from}&date_to=${to}&xvid[0]=${xvid}&domain=${domain}`);
            const json = await response.json();

            dispatch({
                type: FETCH_PROGRAMS,
                payload: json[xvid]
            });
            dispatch(resetNegativeOffset());

        } catch (e) {
            console.log(e);
        }
    }
}

export const clearPrograms = () => {
    return dispatch => {
        dispatch({
            type: CLEAR_PROGRAMS
        });
    }
}
