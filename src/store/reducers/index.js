import {combineReducers} from 'redux';
import {channelsReducer} from './channelsReducer';
import {programsReducer} from './programsReducer';
import {appReducer} from './appReducer';

export const rootReducer = combineReducers({
     channels: channelsReducer,
     programs: programsReducer,
     app: appReducer
});