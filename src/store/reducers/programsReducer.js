import {FETCH_PROGRAMS, CLEAR_PROGRAMS} from '../actions/actionTypes';

const initialState = {
    programs: []
}

export const programsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CLEAR_PROGRAMS:
            return {...state, programs: initialState.programs};
        case FETCH_PROGRAMS:
            return {...state, programs: action.payload};
        default:
            return state;
    }

}