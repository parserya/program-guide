import {ADD_NEGATIVE_OFFSET, RESET_NEGATIVE_OFFSET, TOGGLE_COLLAPSE, SET_CURRENT_DATE} from '../actions/actionTypes';

const initialState = {
    negativeOffset: 0,
    collapsed: false,
    currentDate: null
}
export const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case RESET_NEGATIVE_OFFSET:
            return {...state, negativeOffset: initialState.negativeOffset};
        case ADD_NEGATIVE_OFFSET:
            return {...state, negativeOffset: state.negativeOffset += action.payload};
        case TOGGLE_COLLAPSE:
            return {...state, collapsed: !state.collapsed};
        case SET_CURRENT_DATE:
            return {...state, currentDate: action.payload};
        default:
            return state;
    }
}