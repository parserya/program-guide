import {FETCH_CHANNELS, FETCH_THEMES, BAD_REQUEST} from '../actions/actionTypes';

const initialState = {
    channels: {},
    themes: {},
    badRequest: false
}

export const channelsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CHANNELS:
            return {...state, channels: {...state.channels, ...action.payload}};
        case FETCH_THEMES:
            return {...state, themes: action.payload};
        case BAD_REQUEST:
            return {...state, badRequest: true};
        default:
            return state;
    }

}