import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {useRoutes} from './pages/routes';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import {rootReducer} from './store/reducers';

import dayjs from 'dayjs';
import 'dayjs/locale/ru';
dayjs.locale('ru');

const store = createStore(rootReducer,
    applyMiddleware(
        thunk
    )
);

function App() {
    const routes = useRoutes();
    return (
        <Provider store={store}>
            <Router>
                <div className="app">
                    {routes}
                </div>
            </Router>
        </Provider>
    );
}

export default App;
