import React from 'react';
import ChannelHeader from '../ChannelHeader';
import Loading from '../Loading';
import Footer from '../Footer';

import './index.scss';

export default ({channels, chid, thid, city}) => {
    if (!city || !channels[city] || !channels[city][chid]) return <Loading/>;

    const channel = channels[city][chid];
    return <>
        <ChannelHeader
            channel={channel}
            chid={chid}
            thid={thid}
            city={city}
        />
        <div className="b-channel-block">
            <h2 className="b-channel-block__title">О канале</h2>
            <p className="b-channel-block__description">{channel.description}</p>
            <a href={channel.url} className="b-channel-block__link" target="_blank" rel="noopener noreferrer">Официальный сайт</a>
        </div>
        <Footer
            city={city}
            thid={thid}
            channelTitle={channel.title}
            chid={chid}
        />
    </>
}