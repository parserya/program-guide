import React, {useEffect, useRef} from 'react';
import dayjs from 'dayjs';
import classNames from 'classnames/bind';

import './index.scss';
import {useDispatch} from 'react-redux';
import {addNegativeOffset} from '../../store/actions/app';

export default ({program}) => {
    const refRow = useRef();
    const dispatch = useDispatch();

    const {start, duration, title} = program;

    const startTime = dayjs(start);
    const endTime = dayjs(start).add(duration, 'second');
    const startTimeFormatted = startTime.format("HH:mm");

    const isOver = !dayjs().isBefore(endTime);
    const isInProgress = dayjs().isAfter(startTime) && dayjs().isBefore(endTime);
    let progression = 0;
    if (isInProgress) {
        progression =  Math.floor((dayjs().unix() - startTime.unix()) / +duration * 100);
    }

    useEffect(() => {
        if (isOver) {
            dispatch(addNegativeOffset(refRow.current.clientHeight));
        }
    }, [dispatch, isOver])

    return <div className={classNames({
        "b-program-row": true,
        "b-program-row_is-over": isOver,
        "b-program-row_in-progression": progression > 0
    })}
        ref={refRow}
    >
        <div className="b-program-row__start">
            {startTimeFormatted}
        </div>
        <div className="b-program-row__title">
            {title}
        </div>
        { progression > 0 && <div className="b-program-row__progression">
            <div className="b-program-row__progression-current" style={{width: `${progression}%`}}></div>
        </div>}
    </div>
}