import React from 'react';
import {API_URL} from '../../config';
import {Link} from 'react-router-dom';

import './index.scss';

export default ({channel, chid, thid, city}) => (
    <header className="b-channel-header">
        <div className="b-channel-header__block">
            {channel && channel.logo &&
            <img src={`${API_URL}${channel.logo}`} alt="" className="b-channel-header__logo"/>}
            <Link to={`/${city}/${thid}/${chid}/info`} className="b-channel-header__link">{channel.title}</Link>
        </div>
    </header>
)