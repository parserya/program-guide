import React, {useEffect, useState} from 'react';
import dayjs from 'dayjs';
import classNames from 'classnames/bind';
import {useDispatch} from 'react-redux';

import './index.scss';
import {setCurrentDate} from '../../store/actions/app';

export default ({currentDate}) => {
    const [weekMap, setWeekMap] = useState([]);
    const dispatch = useDispatch();

    useEffect(() => {
        const newWeekMap = [-2, -1, 0, 1, 2, 3, 4];
        for (const key in newWeekMap) {
            const calcDate = dayjs().add(newWeekMap[key], 'day');
            newWeekMap[key] = {
                key: newWeekMap[key],
                date: calcDate,
                value: ((newWeekMap[key] === 0) ? 'Сегодня' : calcDate.format('dd, DD')
                )
            }
        }
        setWeekMap(newWeekMap);
    }, []);

    const datePickHandler = (date) => {
        dispatch(setCurrentDate(date.format('YYYY-MM-DD')));
    }

    return <div className="b-datepicker">
        <div className="b-datepicker__block">
            {Object.values(weekMap).map(weekDay =>
                <button
                    key={weekDay.key}
                    className={classNames({
                        'b-datepicker__button': true,
                        'b-datepicker__button_current':
                            weekDay.date.format('YYYY-MM-DD') === dayjs(currentDate).format('YYYY-MM-DD')
                    })}
                    onClick={() => datePickHandler(weekDay.date)}
                >{weekDay.value}</button>)
            }
        </div>
    </div>
}