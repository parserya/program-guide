import React from 'react';
import {Link, useHistory} from 'react-router-dom';

import {API_URL} from '../../config';
import Loading from '../Loading';

import './index.scss';;

export default ({city, thid, channels, themes}) => {
    const history = useHistory();

    if (!city || !channels[city] || themes.length === 0) return <Loading/>;

    const selectHandler = (event) => {
        if (event.target && event.target.value)
            history.push(`/${city}/${event.target.value}`);
    }

    return <>
        <div className="b-themes">
            <label htmlFor="themes-select" className="b-themes__label">Выберите тему:</label>
            <select
                onChange={selectHandler}
                className="b-themes__select"
                value={thid}
                id="themes-select"
            >
                {Object.values(themes).map(theme =>
                    <option
                        value={theme.value}
                        key={theme.value}
                    >{theme.label}</option>)}
            </select>
        </div>
        <h2>Программа передач</h2>
        <div className="b-channels">
            {Object.values(channels[city]).map(channel =>
                <div className="b-channels__row" key={channel.chid}>
                    <Link to={`/${city}/${thid}/${channel.chid}`}>
                        {channel.logo ?
                            <img
                                src={`${API_URL}${channel.logo}`}
                                alt={channel.title}
                                title={channel.title}
                                className="b-channels__img"
                            /> :
                            <span className="b-channels__noimg">{channel.title}</span>
                        }
                    </Link>
                </div>
            )}
        </div>
    </>
}