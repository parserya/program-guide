import React from 'react';
import Loader from './loading.gif';

export default () => (
    <div className="loading">
        <img src={Loader} alt="Загрузка.." />
    </div>
)
