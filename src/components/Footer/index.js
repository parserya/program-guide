import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {useDispatch} from 'react-redux';

import {ReactComponent as ArrowUp} from './up.svg';
import {ReactComponent as ArrowDown} from './down.svg';
import {toggleCollapse} from '../../store/actions/app';
import './index.scss';

export default ({channelTitle, chid, city, negativeOffset, isCollapsed, thid}) => {
    const [collapsed, setCollapsed] = useState(isCollapsed);
    const dispatch = useDispatch();

    const toggleHandler = (e) => {
        dispatch(toggleCollapse());
        setCollapsed(!collapsed);
    }

    return <footer className="b-toggle-visible">
        <div className="b-toggle-visible__block">
            <div className="b-breadcrumbs">
                <Link to={`/${city}/${thid}`} className="b-breadcrumbs__link">Программа передач</Link> /&nbsp;
                { chid ?
                    <Link to={`/${city}/${thid}/${chid}`} className="b-breadcrumbs__link">{channelTitle}</Link> :
                    <span>{channelTitle}</span>
                }
            </div>
            {(negativeOffset > 0) && (collapsed ?
                    <>
                        <button onClick={toggleHandler} className="b-toggle-visible__link">Развернуть</button>
                        <ArrowDown className="b-toggle-visible__icon"/>
                    </> :
                    <>
                        <button onClick={toggleHandler} className="b-toggle-visible__link">Свернуть</button>
                        <ArrowUp className="b-toggle-visible__icon"/>
                    </>
            )}
        </div>
    </footer>
}