import React from 'react';

import DatePicker from '../DatePicker';
import ChannelHeader from '../ChannelHeader';
import Loading from '../Loading';
import ProgramRow from '../ProgramRow';
import Footer from '../Footer';

import './index.scss';

export default ({channels, programs, chid, thid, city, negativeOffset, collapsed, currentDate}) => {
    if (!city || !programs.length) return <Loading/>;

    const channel = channels[city][chid];

    return <>
        <DatePicker
            currentDate={currentDate}
        />
        <ChannelHeader
            chid={chid}
            city={city}
            thid={thid}
            channel={channel}
        />
        <div
            className="b-programs"
            style={{marginTop: collapsed ? -negativeOffset : 0}}
        >
            {
                programs.map(program => <ProgramRow
                    key={`${program.start}-${program.title}`}
                    program={program}
                />)
            }
        </div>
        <Footer
            isCollapsed={collapsed}
            channelTitle={channel.title}
            negativeOffset={negativeOffset}
            thid={thid}
            city={city}
        />
    </>
}